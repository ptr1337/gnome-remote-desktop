# Maintainer: Fabian Bornschein <fabiscafe-at-mailbox-dot-org>
# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

pkgname=gnome-remote-desktop
pkgver=46.0
pkgrel=2
pkgdesc="GNOME Remote Desktop server"
url="https://wiki.gnome.org/Projects/Mutter/RemoteDesktop"
arch=(x86_64)
license=(GPL-2.0-or-later)
depends=(
  cairo
  dconf
  freerdp
  fuse3
  gcc-libs
  glib2
  glibc
  libdrm
  libei
  libepoxy
  libfdk-aac
  libnotify
  libpipewire
  libsecret
  libvncserver
  libxkbcommon
  opus
  pipewire
  polkit
  systemd
  systemd-libs
  tpm2-tss
)
makedepends=(
  asciidoc
  ffnvcodec-headers
  git
  meson
)
checkdepends=(
  dbus-broker
  libegl
  mutter
  python-dbus
  python-gobject
  wireplumber
)
groups=(gnome)
_commit=9040533be31d11cd5a5174c86bfdc1f39c1c3599  # tags/46.0^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-remote-desktop.git#commit=$_commit")
b2sums=('b0ce3579ce888f84c30d0e86e514d64b42eb9c0bfa59d46e8e1f2efce83c8c9f666a8b3ec3a37f7c88911378d6d581ec960295125cfe7fea26250314653f8f07')

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  cd $pkgname
}

build() {
  local meson_options=(
    -D vnc=true
  )

  arch-meson $pkgname build "${meson_options[@]}"
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs -t 3
}

package() {
  meson install -C build --destdir "$pkgdir"
}

# vim:set sw=2 sts=-1 et:
